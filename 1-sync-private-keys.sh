#!/bin/bash

set -xe

if [ -f ".vagrant/machines/master/virtualbox/private_key" ]; then
    rm .vagrant/machines/master/virtualbox/private_key
    cp config/keys/id_rsa .vagrant/machines/master/virtualbox/private_key
fi

if [ -f ".vagrant/machines/node1/virtualbox/private_key" ]; then
    rm .vagrant/machines/node1/virtualbox/private_key
    cp config/keys/id_rsa .vagrant/machines/node1/virtualbox/private_key
fi

if [ -f ".vagrant/machines/node2/virtualbox/private_key" ]; then
    rm .vagrant/machines/node2/virtualbox/private_key
    cp config/keys/id_rsa .vagrant/machines/node2/virtualbox/private_key
fi

if [ -f ".vagrant/machines/node3/virtualbox/private_key" ]; then
    rm .vagrant/machines/node3/virtualbox/private_key
    cp config/keys/id_rsa .vagrant/machines/node3/virtualbox/private_key
fi