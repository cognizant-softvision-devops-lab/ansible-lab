# Plant A DevOPS

The purpose of the current document is to present a hands-on learning plan for new DevOPS trainees. All the tasks can be performed from a local computer without needing to interact with cloud providers, and they should suffice in laying a solid foundation in regards to technologies and terminologies which will be used in real case scenarios.

The idea is to learn as much as possible while performing the tasks below. Feel free to seek guidance and advice from your trainers and don’t be afraid to use your favourite search engine to look for examples on this crazy invention called the Internet.

Any feedback is more than welcomed via our DevOPS community.

Have fun!

## Level 1

* Create a Virtual Machine using VirtualBox.
* Install any distribution of Linux OS on the newly created Virtual Machine.

If you are new to Linux, take your time and explore. Learn some basic Linux commands. Play around and don’t be afraid of breaking anything.

## Level 2

* Install Vagrant. Repeat the previous process with Vagrant and VirtualBox. Take your time and understand all the pieces and commands involved.
* Install Docker. Run a Docker container with a Linux OS on it. Again, take your time to understand all the steps which are involved. Do some research. What is Docker? What are containers?

## Level 3

* Install Ansible on your workstation.
* Install Apache/Nginx on one of your VirtualBox machines using Ansible. Provisioning can be done either from your Ansible CLI client or from the Ansible provisioner available in Vagrant.
* Install an entire LAMP/LEMP stack on your Vagrant box using Ansible. Configure your virtual machine so that you can see the default Apache page in your workstation’s browser (host machine, not guest).
* Create a simple PHP page. In a new playbook called deploy, send your PHP page to the freshly provisioned virtual machine and configure a virtual host in Apache so that when you access the host you land on your new page instead of the default Apache page.

## Level 4	

* Set up an environment with Vagrant consisting of two boxes with two different Linux distributions: CentOS and Ubuntu.
* Use Ansible to install the LAMP/LEMP stack on both machines at the same time. Use Ansible roles and include the different OS logic within the same role (for example, do not create an Apache role for Ubuntu and another Apache role for CentOS).
* Update and improve your deploy playbook to send your PHP page and configure the Apache virtual host at the same time. Your final goal is that after the playbook runs successfully, you will see your page on both hosts.

## Level 5

* Create a new machine, any OS, using Vagrant.
* Create a new Ansible role for installing Jenkins on the new machine.
In Jenkins, create a job to use the Ansible deploy playbook and deploy the PHP page on the other two boxes (don’t forget to install Ansible on the Jenkins box also). Use SSH keys to connect between Jenkins and your LAMP/LEMP boxes.

## Level 6	

* Create a new virtual machine and install a Graphite server. Pick your favourite Linux distribution as a base OS.
Install a collectd agent on all the virtual machines created so far (2 LAMP, Jenkins, Graphite server)
* Configure collectd to speak with your Graphite server and start sending metrics such as system load, available memory etc.
* Install Grafana on the same box as the Graphite server and configure it to read data from the Graphite database. Create some dashboards and play around! Grafana is really fun!

## Bonus Level

* Create email alerts in Grafana for when the 15s average system load on the LAMP machines passes the value.
* Figure out a way to trigger an alert from any of your LAMP machines by accessing your PHP page using HTTP requests. Be creative!
