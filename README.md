
# Ansible Lab.
In this lab you'll learn to use Ansible and play your first playbook. You have a short description of Ansible and a getting started section
with steps to perform in order to prepare your environment for the lab.

## What is Ansible.
Ansible is an open-source automation tool, or platform, used for IT tasks such as configuration management,
application deployment, intraservice orchestration and provisioning. Automation is crucial these days, with IT environments
that are too complex and often need to scale too quickly for system administrators and developers to keep up if they had to do everything manually.
Automation simplifies complex tasks, not just making developers’ jobs more manageable but allowing them to
focus attention on other tasks that add value to an organization. In other words, it frees up time and increases efficiency.
And Ansible, as noted above, is rapidly rising to the top in the world of automation tools. Let’s look at some of the reasons for Ansible’s popularity.

## Advantages.
* Free. Ansible is an open-source tool.
* Very simple to set up and use. No special coding skills are necessary to use Ansible’s playbooks (more on playbooks later).
* Powerful. Ansible lets you model even highly complex IT workflows.
* Flexible. You can orchestrate the entire application environment no matter where it’s deployed. You can also customize it based on your needs.
* Agentless. You don’t need to install any other software or firewall ports on the client systems you want to automate.
    You also don’t have to set up a separate management structure.
* Efficient. Because you don’t need to install any extra software, there’s more room for application resources on your server.

## Playbooks.
Ansible playbooks are like instruction manuals for tasks. They are simple files written in YAML, which stands for YAML Ain’t Markup Language,
a human-readable data serialization language. Playbooks are really at the heart of what makes Ansible so popular because they
describe the tasks to be done easily and without the need for the user to know or remember any special syntax. Not only can they declare
configurations, but they can orchestrate the steps of any manually ordered task, and can execute tasks at the same time or at different times.

Each playbook is composed of one or multiple plays, and the goal of a play is to map a group of hosts to well-defined roles, represented by tasks.

## Getting started with the virtual environment.

1. Execute `Vagrantfile` to build the master and node virtual machines.
   * `vagrant up` to build/start the vm's.
   * `vagrant halt` to stop the vm's.
   * `vagrant destroy` to remove all the vm's.
   * You can build/start/stop/destroy individual vm's by adding an argument to the command. ex: `vagrant up <argument>`.
        where the argument is the name of the vm.

2. Connect to each vm using `vagrant ssh <argument>` and run the `script-ssh-sync.sh` file. This script will replace the original shs keys inside vm's.
3. Exit vm's and run the `1-sync-private-keys.ssh` file. This script will replace de ssh private keys of the vm's.

Vagrant cheatsheet.[https://gist.github.com/wpscholar/a49594e2e2b918f4d0c4]

## Getting started with the ansible.
After connecting to `master` we need to execute `ansible -m ping all` to make sure all the nodes are reachable.

1. `ansible-playbook playbook.yml` to play your ansible playbook.
2. You can use `ansible-lint` to run a detail check of your playbooks before you execute them.
    For example, if you run ansible-lint on the `single_playbook.yml` playbook, you’ll get the following results:
    ```bash
    [ANSIBLE0010] Package installs should not use latest
    single_playbook.yml:8
    Task/Handler: Installs Apache Web Server

    [ANSIBLE0010] Package installs should not use latest
    single_playbook.yml:12
    Task/Handler: Installs Apache Web Server

    [ANSIBLE0013] Use shell only when shell functionality is required
    single_playbook.yml:16
    Task/Handler: httpd service enable and start centos

    [ANSIBLE0013] Use shell only when shell functionality is required
    single_playbook.yml:23
    Task/Handler: httpd service enable and start Ubuntu

    .....
    ```
3. `ansible-playbook main.yml -t php-demo-run` will execute the main file but only the tag specified in.

### Ansible roles.
Roles are ways of automatically loading certain vars_files, tasks, and handlers based on a known file structure.
Grouping content by roles also allows easy sharing of roles with other users.

Role Directory Structure:
```bash
site.yml
webservers.yml
fooservers.yml
roles/
   common/
     tasks/
     handlers/
     files/
     templates/
     vars/
     defaults/
     meta/
   webservers/
     tasks/
     defaults/
     meta/
```

Roles expect files to be in certain directory names. Roles must include at least one of these directories,
however it is perfectly fine to exclude any which are not being used. When in use,
each directory must contain a `main.yml` file, which contains the relevant content:

* `tasks` - contains the main list of tasks to be executed by the role.
* `handlers` - contains handlers, which may be used by this role or even anywhere outside this role.
* `defaults` - default variables for the role (see Using Variables for more information).
* `vars` - other variables for the role (see Using Variables for more information).
* `files` - contains files which can be deployed via this role.
* `templates` - contains templates which can be deployed via this role.
* `meta` - defines some meta data for this role.