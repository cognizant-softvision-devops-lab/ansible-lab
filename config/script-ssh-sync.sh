#!/bin/bash

set -xe

echo -e "\n---- copy ssh keys from sync folder ----"
rm /home/vagrant/.ssh/authorized_keys
cp /home/vagrant/config/keys/id_rsa.pub /home/vagrant/.ssh/authorized_keys
cp /home/vagrant/config/keys/id_rsa /home/vagrant/.ssh/id_rsa

echo -e "\n---- ssh permisions set ----"
chmod 0755 /home/vagrant/.ssh
chmod 0600 /home/vagrant/.ssh/authorized_keys
chmod 0600 /home/vagrant/.ssh/id_rsa
chown -R vagrant:vagrant /home/vagrant/.ssh

